1. Tutorial python:...................Simulación.ipynb
2. Tutorial Git.......................Git.png
3. Sim juego dados....................Simulación.ipynb
4. Sim regresión pandemia.............Probabilidad-Regresión/ProbabilidadRegresion.ipynb
5. Sim regresión 2 SIR................Modelo-SIR/SImulacionSIR.ipynb
6. Sim regresión vs probabilidad......Probabilidad-Regresión/ProbabilidadRegresion.ipynb
7. Sim pygame-SIR.....................Modelo-PyGame/SimulacionContactosR0.html
8. Dis-covid-hospital.................Probabilidad-Eventos-Discretos/SimuladorEventosSympyParte2.ipynb
9. Dis-ajuste variables hospital......Probabilidad-y-estadística-con-python/AjusteVariablesPython.ipynb